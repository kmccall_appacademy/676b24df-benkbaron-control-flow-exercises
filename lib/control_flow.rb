# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  upper_case_str = ""
  str.chars.each {|ltr| upper_case_str << ltr unless ltr == ltr.downcase}
  upper_case_str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[str.length / 2]
  else
    str[(str.length / 2) - 1..(str.length / 2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = "aeiou"
  str.downcase.count(vowels)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num == 1
    return 1
  end
  return num * factorial(num-1)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
new_str = ""
arr.each_with_index do |el, idx|
  new_str << el
  new_str << separator if idx != arr.length - 1
end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  weird_str = ""
  str.chars.each_with_index do |ltr, idx|
    idx.odd? == true ? weird_str << ltr.upcase : weird_str << ltr.downcase
  end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str_arr = str.split
  str_arr.map! do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
end
  str_arr.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  arr = []

  (1..n).each do |num|
    if num % 15 == 0
      arr << "fizzbuzz"
    elsif num % 5 == 0
      arr << "buzz"
    elsif num % 3 == 0
      arr << "fizz"
    else
      arr << num
    end
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  rev_arr = []
  arr.each {|el| rev_arr = [el] + rev_arr}
  rev_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1

  (2...num).each {|test_factor|  return false if num % test_factor == 0}

  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factor_arr = []
  (1..num).each do |test_factor|
    factor_arr << test_factor if num % test_factor == 0
  end
  factor_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor_arr = factors(num)
  prime_factors = []
  factor_arr.each {|factor| prime_factors << factor if prime?(factor)}
  prime_factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each {|num| num.odd? == true ? odds << num : evens << num}

  odds.length == 1 ? odds[0] : evens[0]
end
